<?php
require_once('stemming.php');
include './JaroWinkler.php';

$my_text=file_get_contents("dictionary/IndoWordList.dic");

$dictionary  = preg_split('/\r\n|\r|\n/', $my_text);

$jml_kata = count($dictionary);

$target = $_GET['text'];

$unwantedChars = array(',', '!', '?', '.', '`', "'", '"', '/', '<', '>','[', ']','{','}','|','@','#','$','%','^','&','*','(',')','_','+','=',';',':');

// tokenizing
//$words = array(str_word_count(preg_replace('/-/', ' ', $target), 1));
$words = explode(" ", $target);
$words_original = explode(" ", $target); //kata original yg masih ada tanda baca
$words_stemmed = array(); // kata yang di stem
$words_original2 = array(); //kata original untuk mengilangkan tanda baca

$corrected_word = array(); //kata yang sudah dikoreksi

//proses stemming
for ($i=0; $i < count($words); $i++) { 
    $words[$i] = str_replace($unwantedChars, '', $words[$i]);
    array_push($words_stemmed, stemming($words[$i]));
}

for ($i=0; $i < count($words); $i++) { 
    $words[$i] = str_replace($unwantedChars, '', $words[$i]);
    array_push($words_original2, strtolower($words[$i]));
}

$missword_nonstem = array(); //kata salah yang tidak distem
$missword = array(); //kata salah yang sudah distem

$correct_word = array(); //kata benar yang sidah di stem
$correct_word_nonstem = array(); //kata benar yang tidak di stem

$result = array();
$result2 = array();

$missword_location = array();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <!-- Morris chart -->
    <link rel="stylesheet" href="morris.js/morris.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
                </nav>
                <br>
            </div>
            <div class="col-md-4">
                <form action="jw_process.php" method="get">
                    <div class="form-group">
                        <label for="">Output</label>
                        <p>
                            <?php
                                $count_target = count($words_stemmed);
                                for ($i=0; $i < $count_target; $i++) 
                                { 
                                    if(preg_match('/[A-Z]/', $words_stemmed[$i]))
                                    {
                                        $words_stemmed[$i] = strtolower($words_stemmed[$i]);
                                        
                                        $words_stemmed[$i] = str_replace($unwantedChars, '', $words_stemmed[$i]);
                                        if(preg_match('/[0-9]/', $words_stemmed[$i]))
                                        {
                                            echo $words_original[$i]." ";
                                        } else {
                                            if (in_array($words_stemmed[$i], $dictionary)) 
                                            {
                                                echo $words_original[$i]." ";
                                                array_push($correct_word, $words_stemmed[$i]);
                                                array_push($correct_word_nonstem, $words_original[$i]);
                                            } elseif (in_array($words[$i], $dictionary)) {
                                                echo $words_original[$i]." ";
                                                array_push($correct_word, $words_stemmed[$i]);
                                                array_push($correct_word_nonstem, $words_original[$i]);
                                            } else {
                                                echo "<b style='background: red;' onclick='myFunction()'>".$words_original[$i]."</b>"." ";
                                                array_push($missword, $words_stemmed[$i]);
                                                array_push($missword_nonstem, $words_original[$i]);
                                            }
                                        }
                                    } else {
                                        $words_stemmed[$i] = str_replace($unwantedChars, '', $words_stemmed[$i]);
                                        if(preg_match('/[0-9]/', $words_stemmed[$i]))
                                        {
                                            echo $words_original[$i]." ";
                                        } else {
                                            if (in_array($words_stemmed[$i], $dictionary)) 
                                            {
                                                echo $words_original[$i]." ";
                                                array_push($correct_word, $words_stemmed[$i]);
                                                array_push($correct_word_nonstem, $words_original[$i]);
                                            } elseif (in_array($words[$i], $dictionary)) {
                                                echo $words_original[$i]." ";
                                                array_push($correct_word, $words_stemmed[$i]);
                                                array_push($correct_word_nonstem, $words_original[$i]);
                                            } else {
                                                echo "<b style='background: red;' onclick='myFunction()'>".$words_original[$i]."</b>"." ";
                                                array_push($missword, $words_stemmed[$i]);
                                                array_push($missword_nonstem, $words_original[$i]);
                                            }
                                        }
                                    }
                                }
                            ?>
                        </p>
                    </div>
                </form>
                <form action="jw_process.php" method="get">
                    <div class="form-group">
                        <label for="">Autocorrect</label>
                        <p>
                            <?php
                                $count_target = count($words_stemmed);
                                for ($i=0; $i < $count_target; $i++) 
                                { 
                                    if(preg_match('/[A-Z]/', $words_stemmed[$i]))
                                    {
                                        $words_stemmed[$i] = strtolower($words_stemmed[$i]);
                                        
                                        $words_stemmed[$i] = str_replace($unwantedChars, '', $words_stemmed[$i]);
                                        if(preg_match('/[0-9]/', $words_stemmed[$i]))
                                        {
                                            echo $words_original[$i]." ";
                                        } else {
                                            if (in_array($words_stemmed[$i], $dictionary)) 
                                            {
                                                echo $words_original[$i]." ";
                                            } elseif (in_array($words[$i], $dictionary)) {
                                                echo $words_original[$i]." ";
                                            } else {
                                                array_push($missword_location, $i);
                                                echo "<b style='background: green;' id='autocorrect".$i."'>".$words_original[$i]."</b>"." ";
                                            }
                                        }
                                    } else {
                                        $words_stemmed[$i] = str_replace($unwantedChars, '', $words_stemmed[$i]);
                                        if(preg_match('/[0-9]/', $words_stemmed[$i]))
                                        {
                                            echo $words_original[$i]." ";
                                        } else {
                                            if (in_array($words_stemmed[$i], $dictionary)) 
                                            {
                                                echo $words_original[$i]." ";
                                            } elseif (in_array($words[$i], $dictionary)) {
                                                echo $words_original[$i]." ";
                                            } else {
                                                array_push($missword_location, $i);
                                                echo "<b style='background: green;' id='autocorrect".$i."'>".$words_original[$i]."</b>"." ";
                                            }
                                        }
                                    }
                                }
                            ?>
                        </p>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <label for="">Daftar kesalahan Kata : </label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kata Salah</th>
                                <th>Sugesti Kata</th>
                                <th>Persentase Kesalahan</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($missword as $key=>$mw): ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $mw; ?></td>
                                <td>
                                    <?php
                                        //Proses Perhitungan Jaro Winkler
                                        for ($i=0; $i < $jml_kata; $i++) 
                                        {
                                            $newdata = array('word'=>$dictionary[$i], 'jw_val'=>JaroWinkler($mw, $dictionary[$i]));
                                            array_push($result, $newdata);
                                        }

                                        //Sort dari nilai JW yang paling besar
                                        $keys = array_column($result, 'jw_val');
                                        array_multisort($keys, SORT_DESC, $result);
                                        
                                        //echo max(array_column($result, 'word'));

                                        //Menampilkan Sugesti Kata
                                        $j = 0;
                                        foreach ($result as $key=>$res) {
                                            echo $res['word']."<br>";
                                            if ($j == 0) {
                                                array_push($result2, array('word'=>$res['word'], 'jw_val'=>$res['jw_val']));
                                            }
                                            if ($j == 5) {
                                                break;
                                            }
                                            $j++;
                                        }
                                        
                                        $result = array();
                                    ?>
                                </td>   
                                <td>
                                <?php
                                        //Proses Perhitungan Jaro Winkler
                                        for ($i=0; $i < $jml_kata; $i++) 
                                        {
                                            $newdata = array('word'=>$dictionary[$i], 'jw_val'=>JaroWinkler($mw, $dictionary[$i]));
                                            array_push($result, $newdata);
                                        }

                                        //Sort dari nilai JW yang paling besar
                                        $keys = array_column($result, 'jw_val');
                                        array_multisort($keys, SORT_DESC, $result);

                                        //Menampilkan Sugesti Kata
                                        $j = 0;
                                        foreach ($result as $key=>$res) {
                                            echo 100 - ($res['jw_val'] * 100)." % <br>";
                                            if ($j == 5) {
                                                break;
                                            }
                                            $j++;
                                        }

                                        $result = array();
                                    ?>
                                </td>
                            </tr>
                            <?php $no++; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
            </div>
            <div class="col-md-4">
                <div id="myfirstchart" style="height: 250px;"></div>
            </div>
        </div>
    </div>
</body>
</html>
<?php
        $k = 0;
        foreach ($result2 as $key => $res2) {
            echo '<script> document.getElementById("autocorrect'.$missword_location[$k].'").textContent="'.$res2['word'].'";</script>';
            //echo $res2['word']."<br>";
            $k++;
        }
?>
<?php
    $jml_kata = count($words_original2);
    $jml_kata_salah = count($missword);
    $jml_kata_benar = count($correct_word);

    $percent_jml_kata_benar = ($jml_kata_benar/$jml_kata)*100;
    $percent_jml_kata_salah = ($jml_kata_salah/$jml_kata)*100;
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<!-- Morris.js charts -->
<script src="raphael/raphael.min.js"></script>
<script src="morris.js/morris.min.js"></script>
<script>
    new Morris.Donut({
    // ID of the element in which to draw the chart.
    element: 'myfirstchart',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [
        { label: 'Typo', value: <?php echo $percent_jml_kata_salah; ?> },
        { label: 'Kata Benar', value: <?php echo $percent_jml_kata_benar; ?> },
    ],
    formatter: function (value) { return value + "%" },
    // The name of the data record attribute that contains x-values.
    xkey: 'year',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['value'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Value']
    });
</script>